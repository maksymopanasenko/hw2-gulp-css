window.addEventListener('DOMContentLoaded', () => {
    const burger = document.querySelector('.header__burger'),
          dropList = document.querySelector('.header__drop-down'),
          btnContainer = burger.firstChild;

    burger.addEventListener('click', () => {
        toggleBurger();
    });

    dropList.addEventListener('click', (e) => {
        const target = e.target;
        
        if (target.nodeName != "A") return false;

        const options = dropList.childNodes;
        options.forEach(item => item.firstChild.classList.remove('header__option-link_active'));
        target.classList.add('header__option-link_active');

        toggleBurger();
    });

    function toggleBurger() {
        dropList.classList.toggle('hide');
        btnContainer.children[0].classList.toggle('hide');
        btnContainer.children[1].classList.toggle('rotate-left');
        btnContainer.children[2].classList.toggle('rotate-right');
    }
});